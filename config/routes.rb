Rails.application.routes.draw do
  get '/main/input', to: 'main#input'
  get '/main/result', to: 'main#result'
  get '/input', to: redirect('/main/input')
  get '/result', to: redirect('/main/result')
  get '/', to: redirect('/main/input')

  # Defines the root path route ("/")
  # root "articles#index"
end
