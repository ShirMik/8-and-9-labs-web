# frozen_string_literal: true

require 'spec_helper'
require 'rails_helper'
require_relative '../../app/controllers/main_controller'

RSpec.describe Calcer do
  describe 'input validation' do
    it 'for one fixed calculated value' do
      expect(Calcer.calc(6)).to eq([6, 63])
    end
    it 'scam' do
      expect(Calcer.calc('6')).to be_nil
    end
  end
end
