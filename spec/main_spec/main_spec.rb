# frozen_string_literal: true

require 'spec_helper'
require 'rails_helper'

RSpec.describe MainController, type: :controller do
  context 'GET' do
    it 'has a 200 status code for /main/input' do
      get 'input'
      expect(response.status).to eq(200)
    end

    it 'has a 200 response code for /main/result' do
      get 'result'
      expect(response.status).to eq(200)
    end
  end

  context 'check value' do
    it '' do
      cl = described_class.new
      cl.params = {
        p: 6
      }
      cl.result
      expect(cl.result).to eq([
        [1, 1],
        [2, 3],
        [3, 7],
        [4, 15],
        [6, 63]
      ])
    end
  end
end
