# frozen_string_literal: true

class MainController < ApplicationController
  def input() end

  def result
    limit = params[:p].to_i
    @result = (1..limit).collect { |x| Calcer.calc x }
                        .take_while { |i| i[0].nil? || (i[0] <= limit) }
                        .filter { |i| i[0].nil? ? nil : i }
  end
end

# class that calculates Mersen's value
class Calcer
  def self.calc(p)
    return unless p.is_a? Integer

    r = (2**p - 1)
    simple?(p) ? [p, r] : [nil, nil]
  end

  def self.simple?(number)
    return unless number.is_a? Integer

    (1..number / 2).all? { |i| ((number / i).to_f - number.to_f / i).zero? }
  end
end
